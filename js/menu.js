var menuState = {

	create: function (){
		var txtPalestra = game.add.text(game.world.centerX,150,'PALESTRA', {font:'40px emulogic', fill:'#fff'});
		txtPalestra.anchor.set(.5); //ponto de ancoragem do texto .5 significa o mesmo que (0.5,0.5)


		var txtPressStart = game.add.text(game.world.centerX,550,'PRESS START', {font:'20px emulogic', fill:'#fff'});
		txtPressStart.anchor.set(.5); 

		game.add.tween(txtPressStart).to({y:250},1000).start();

		game.time.events.add(1000, function(){
			var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
			enterKey.onDown.addOnce(this.startGame, this)	
			game.input.onDown.addOnce(this.startGame, this)	
		}, this)

	},

	startGame: function(){
		game.state.start(game.json_data.Config_dict["staring_scene"]);
	}


};
