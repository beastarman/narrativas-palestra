var Scene = function(lines, bg, fade_in, options, option_type, tag)
{
	this.bg = bg;
	this.fade_in = fade_in;
	this.lines = lines;
	this.options = options;
	this.option_type = option_type;
	this.tag = tag;
}

Scene.prototype.create = function()
{
	game.global.currentContent = 0;
	this.onGame = true;

	this.currentAnswer = false;

	this.personagem =[];
	this.personagemSecundario = [];
	game.global.content = [];
	
	if(this.tag)
	{
		game.global.tags.push(this.tag)
	}
	
	game.global.lineIndex = 0;
	
	for (var i=0; i<this.lines.length; i++)
	{
		line = this.lines[i];
		
		split_line = line.text.replace(/^\\+|\\+$/g,"").split("\\\\");
		
		for(var l=0; l<split_line.length; l++)
		{
			split_line[l] = split_line[l].trim();
		}
		
		game.global.content.push(split_line);
		
		this.personagem.push(line.leftsprite)
		
		if(line.rightsprite)
		{
			this.personagemSecundario.push([line.rightsprite,true])
		}
		else
		{
			this.personagemSecundario.push([line.leftsprite,false])
		}
	}
	
	this.spriteInicio = game.add.sprite(0, 0, this.bg);
	this.spritePersonagem = game.add.sprite(100, 100, this.personagem[0]);
	this.spritePersonagemSecundario = game.add.sprite(500, 100, this.personagemSecundario[0][0]);
	
	
	if(this.fade_in)
	{
		this.spriteInicio.alpha = 0;
		this.spritePersonagem.alpha = 0;
		this.spritePersonagemSecundario.alpha = 0;
		game.add.tween(this.spriteInicio).to({ alpha: 1 }, 3000, Phaser.Easing.Linear.None, true);
		game.add.tween(this.spritePersonagem).to({ alpha: 1 }, 3000, Phaser.Easing.Linear.None, true);
		game.add.tween(this.spritePersonagemSecundario).to({ alpha: 1 }, 3000, Phaser.Easing.Linear.None, true);
	}
	else
	{
		this.spriteInicio.alpha = 1;
		this.spritePersonagem.alpha = 1;
		this.spritePersonagemSecundario.alpha = 1;
	}
	
	console.info(this)

	this.spritePersonagemSecundario.visible = this.personagemSecundario[0][1];

	var spritebg = game.add.sprite(game.world.centerX, 410, 'chatbox');
	spritebg.anchor.set(.5);

	game.global.init();
	
	this.text = game.add.text(40, 360, '', {font:'25px TTitilliumWeb-Light', fill:'#fff'});
	game.global.defineText(this.text);
	
	var skipKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
	skipKey.onDown.add(function(){game.global.fast=true},this)
	skipKey.onUp.add(function(){game.global.fast=false},this)
}

Scene.prototype.downChatBox = function (item) {

	this.spritePersonagemSecundario.loadTexture(this.personagemSecundario[game.global.currentContent][0], 0);

	this.spritePersonagemSecundario.visible = this.personagemSecundario[game.global.currentContent][1];

	this.spritePersonagem.loadTexture(this.personagem[game.global.currentContent], 0);

	game.global.resetText();
	game.global.init();
	game.global.nextLine();

},

Scene.prototype.downAnswer = function (item) {
	console.info(item)
	this.onGame = false;
	game.global.fast=false
	game.state.start(this.options[item._text].scene);
},

Scene.prototype.overAnswer = function (item) {
	item.fill = "red";
},

Scene.prototype.outAnswer = function (item) {
	item.fill = "#000";
},

Scene.prototype.update = function () {

	if(this.onGame){

		if (!game.global.loadingText && game.global.currentContent < game.global.content.length) {
			console.info("Add next line");
			game.input.onDown.removeAll()
			
			if(game.global.fast)
			{
				game.time.events.add(0, this.downChatBox, this)
			}
			else
			{
				game.input.onDown.addOnce(this.downChatBox, this);
			}
			
			game.global.loadingText = true;
			
			this.currentAnswer = false
		}

		if(this.currentAnswer == false && game.global.currentContent  === game.global.content.length){
			
			if(Object.keys(this.options).length==0)
			{
				console.info("Add end scene");
				game.input.onDown.removeAll()
				game.input.onDown.addOnce(function()
				{
					game.state.start("end");
				},this)
			}
			else if("" in this.options)
			{
				console.info("Add next scene");
				game.input.onDown.removeAll()
				
				if(game.global.fast)
				{
					game.time.events.add(0, function()
					{
						game.global.fast=false
						game.state.start(this.options[""].scene);
					}, this)
				}
				else
				{
					game.input.onDown.addOnce(function()
					{
						game.state.start(this.options[""].scene);
					},this)
				}
			}
			else
			{
				game.input.onDown.removeAll()
				game.time.events.add(1000, function () {
					
					var balao = this.option_type=="THINK"?'balaoAcao':(this.option_type=="SPEAK"?'balaoFala':'balaoNeutro')
					
					var spriteCel = game.add.sprite(472, 200, balao);
					spriteCel.anchor.set(.5);
					
					var base = 240 - Object.keys(this.options).length*35
					var diff = 0;
					for(var opt in this.options)
					{
						var textAnswer = undefined;
						
						if(this.options[opt].condition && game.global.tags.indexOf(this.options[opt].condition)<0)
						{
							textAnswer = game.add.text(530, base+diff, opt, {font:'20px TTitilliumWeb-Light', fill:'#888'});
							textAnswer.inputEnabled = false;
						}
						else
						{
							textAnswer = game.add.text(530, base+diff, opt, {font:'20px TTitilliumWeb-Light', fill:'#000'});
							textAnswer.inputEnabled = true;
						}
						
						textAnswer.events.onInputDown.add(this.downAnswer, this);
						
						textAnswer.events.onInputOver.add(this.overAnswer, this);
						textAnswer.events.onInputOut.add(this.outAnswer, this);
						
						diff += 70;
					}

					this.spritePersonagemSecundario.visible = false;
				}, this);
			}


			this.currentAnswer = true;

		}

	}
}
