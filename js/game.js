
var game = new Phaser.Game(750, 500, Phaser.AUTO);


game.global = {
	loadingText: true,
	line: [],
	letterIndex: 0,
	lineIndex: 0,
	letterDelay: 0,
	lineDelay: 0,
	fast: false,
	content: [],
	tags: [],

	currentContent: 0,
	textChat: null,
	init: function () {
		
		this.line = [];
		this.letterIndex = 0;
		this.lineIndex = 0;
		this.letterDelay = 0;
		this.lineDelay = 0;
	},

	defineText: function (text) {
		this.textChat = text;
		this.lineIndex = 0;
		this.nextLine();
		
	},

	nextLine: function () {
		
		if (this.currentContent >= this.content.length) {
			this.loadingText = false;
			return;
		}
		
		if (this.lineIndex === this.content[this.currentContent].length) {

			game.global.currentContent++;
			this.loadingText = false;
			this.lineIndex = 0;
			// fim
			return;
		}
		
		this.line = this.content[this.currentContent][this.lineIndex];
		this.letterIndex = 0;

		//  Chame a função 'nextLetter' uma vez pra cada letra na linha (line.length)
		game.time.events.add(this.letterDelay, this.nextLetter, this);
		
	},

	resetText: function(){
		this.textChat.text = "";
	},

	nextLetter: function () {
		
		if(this.line == undefined)
		{
			console.info(this.content)
			console.info(this.currentContent)
			console.info(this.lineIndex)
			console.info("ERROR")
		}
		
		if(this.line[this.letterIndex] != undefined)
		{
			//  Adicione a próxima letra à cadeia de texto
			this.textChat.text = this.textChat.text.concat(this.line[this.letterIndex]);
			//  Avança o índice de letras para a próxima letra na linha
			this.letterIndex++;
		}
		
		if(this.fast)
		{
			for(var i=0; i<10; i++)
			{
				if(this.line[this.letterIndex] != undefined)
				{
					//  Adicione a próxima letra à cadeia de texto
					this.textChat.text = this.textChat.text.concat(this.line[this.letterIndex]);
					//  Avança o índice de letras para a próxima letra na linha
					this.letterIndex++;
				}
			}
		}

		//  ultima letra?
		if (this.letterIndex === this.line.length) {
			this.textChat.text = this.textChat.text.concat("\n");
		
			//  avança para proxima linha
			console.info(this.lineIndex)
			this.lineIndex++;
			
			//  Obter a próxima linha após a quantidade LineDelay de ms ter decorrido
			game.time.events.add(this.lineDelay, this.nextLine, this);
			
		}
		else
		{
			game.time.events.add(this.letterDelay, this.nextLetter, this);
		}
		
	},


};

function load_game(data)
{
	console.info(data)
	game.json_data = data
	
	game.state.add('boot', bootState);

	game.state.add('load', loadState);
	game.state.add('menu', menuState);
	
	var scene_dict = {}
	
	for(var s=0; s<game.json_data.Scenes.length; s++)
	{
		var scene = game.json_data.Scenes[s];
		scene_dict[scene.id] = scene
		
		var options = {}
		
		if("option1scene" in scene)
		{
			scene["option1text"] = scene["option1text"] || ""
			scene["option1text"] = scene["option1text"].replace("\\\\","\n")
			
			options[scene["option1text"] || ""] = {
				"scene": scene["option1scene"],
				"condition": scene["option1condition"],
			}
		}
		
		if("option2scene" in scene && "option2text" in scene)
		{
			scene["option2text"] = scene["option2text"] || ""
			scene["option2text"] = scene["option2text"].replace("\\\\","\n")
			
			options[scene["option2text"]] = {
				"scene": scene["option2scene"],
				"condition": scene["option2condition"],
			}
		}
		
		if("option3scene" in scene && "option3text" in scene)
		{
			scene["option3text"] = scene["option3text"] || ""
			scene["option3text"] = scene["option3text"].replace("\\\\","\n")
			
			options[scene["option3text"]] = {
				"scene": scene["option3scene"],
				"condition": scene["option3condition"],
			}
		}
		
		scene.options = options
		scene.lines = []
	}
	
	game.json_data.Lines.sort(function(a,b){return a.line-b.line})
	
	for(var l=0; l<game.json_data.Lines.length; l++)
	{
		var line = game.json_data.Lines[l];
		
		if(scene_dict[line.scene] == undefined)
		{
			console.info(line.scene);
		}
		
		scene_dict[line.scene].lines.push(line)
	}
	
	game.json_data.Config_dict = {}
	
	for(var c=0; c<game.json_data.Config.length; c++)
	{
		var config = game.json_data.Config[c]
		game.json_data.Config_dict[config.id] = config.value
	}
	
	for(var s=0; s<game.json_data.Scenes.length; s++)
	{
		scene = game.json_data.Scenes[s];
		
		game.state.add(scene.id, new Scene(
			scene.lines,
			scene.background,
			scene.fadein,
			scene.options,
			scene.optiontype,
			scene.acquiretag
		));
	}

	game.state.add('end', endState);

	game.state.start('boot');
}

fetch("data/data.json").then(response => response.json()).then(json => load_game(json));

