var stage1State = {
	
		create: function () {
			game.global.currentContent = 0;
			this.onGame = true;

			this.currentAnswer = false;
	
			this.spriteInicio = game.add.sprite(0, 0, 'bg-inicio');
			this.spriteInicio.alpha = 1;
			//game.add.tween(this.spriteInicio).to({ alpha: 1 }, 3000, Phaser.Easing.Linear.None, true);
	

			this.personagem =[
				"personagem08",
				"personagem09",
				"personagem09",
				"personagem08"
			];

			this.spritePersonagem = game.add.sprite(100, 100, this.personagem[0]);
			this.spritePersonagem.alpha = 1;
			//game.add.tween(this.spritePersonagem).to({ alpha: 1 }, 3000, Phaser.Easing.Linear.None, true);
			
			this.personagemSecundario =[
				
				[
					"personagem08", true
				],
				[
					"celular", true
				],
				[
					"celular", false
				],
				[
					"celular", true
				]

			];

			this.spritePersonagemSecundario = game.add.sprite(500, 100, this.personagemSecundario[0][0]);
			this.spritePersonagemSecundario.alpha = 1;
			//game.add.tween(this.spritePersonagemSecundario).to({ alpha: 1 }, 3000, Phaser.Easing.Linear.None, true);



			this.spritePersonagemSecundario.visible = this.personagemSecundario[0][1];

			var spritebg = game.add.sprite(game.world.centerX, 410, 'chatbox');
			spritebg.anchor.set(.5);

			game.global.init();

			
			game.global.content =[

				[
					"Você diz:",
					"É uma quarta-feira qualquer na primavera",
					"duramente polinizada de Porto Alegre.",
				],
				[
					"Chamada no celular:",
					"RING! RING!"
				
				],
				[
					"RING! RING!",
				
				],
				[
					"Primo diz:",
					"Vamos sair?"
				]

			];

			
			this.text = game.add.text(40, 360, '', {font:'25px TTitilliumWeb-Light', fill:'#fff'});
			game.global.defineText(this.text);

		},

		downChatBox: function (item) {

			this.spritePersonagemSecundario.loadTexture(this.personagemSecundario[game.global.currentContent][0], 0);

			this.spritePersonagemSecundario.visible = this.personagemSecundario[game.global.currentContent][1];

			this.spritePersonagem.loadTexture(this.personagem[game.global.currentContent], 0);

			game.global.resetText();
			game.global.init();
			game.global.nextLine();

		},
		
		downAnswer1: function (item) {
			this.onGame = false;
			game.state.start('stage2');
		},

		downAnswer2: function (item) {
			this.onGame = false;

			game.state.start('stage1');
		},

		overAnswer: function (item) {
			item.fill = "red";
		},

		outAnswer: function (item) {
			item.fill = "#000";
		},
	
		update: function () {

			if(this.onGame){

				if (!game.global.loadingText && game.global.currentContent < game.global.content.length) {
					
					game.input.onDown.addOnce(this.downChatBox, this);
					game.global.loadingText = true;

				}

				if(this.currentAnswer == false && game.global.currentContent  === game.global.content.length){
					
					game.time.events.add(1000, function () {
						
						var spriteCel = game.add.sprite(472, 200, 'balaoAcao');
						spriteCel.anchor.set(.5);

						var textAnswer1 = game.add.text(530, 200, 'SIM', {font:'20px TTitilliumWeb-Light', fill:'#000'});
						var textAnswer2 = game.add.text(530, 230, 'NÃO', {font:'20px TTitilliumWeb-Light', fill:'#000'});
						
						textAnswer1.inputEnabled = true;
						textAnswer2.inputEnabled = true;
						textAnswer1.events.onInputDown.add(this.downAnswer1, this);
						textAnswer2.events.onInputDown.add(this.downAnswer2, this);


						textAnswer1.events.onInputOver.add(this.overAnswer, this);
						textAnswer1.events.onInputOut.add(this.outAnswer, this);

						textAnswer2.events.onInputOver.add(this.overAnswer, this);
						textAnswer2.events.onInputOut.add(this.outAnswer, this);

						this.spritePersonagemSecundario.visible = false;
					}, this);


					this.currentAnswer = true;

				}

			}
		}
	
	
	};
