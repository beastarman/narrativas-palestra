var loadState = {
	preload: function(){
		
		console.info("Running preload")
		
		//carregar todas as imagens do jogo
		
		for(var i=0; i<game.json_data.Images.length; i++)
		{
			var image = game.json_data.Images[i]
			var imagepath = "img/"+image.imagepath
			console.info("Loading " + image.id + " at " + imagepath)
			game.load.image(image.id,imagepath);
		}
		
		var txtLoading = game.add.text(game.world.centerX,150,'LOADING...', {font:'15px emulogic', fill:'#fff'});
		txtLoading.anchor.set(.5); //ponto de ancoragem do texto .5 significa o mesmo que (0.5,0.5)

		var progressBar = game.add.sprite(game.world.centerX,250,'progressBar');
		progressBar.anchor.set(.5); //ponto de ancoragem da imagem .5 significa o mesmo que (0.5,0.5)

		game.load.setPreloadSprite(progressBar); //exibe a barra de progresso


		
		

		//carregar todos os sprites sheet
		//game.load.spritesheet('coin','img/bg1.png', 32,32);
	
	},

	create: function(){
		game.state.start('menu');
	}

};
